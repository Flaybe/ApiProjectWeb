﻿namespace Services.Services.Seeding
{
    public interface IClientSeeder
    {
        public void Seed();
    }
}
