﻿namespace Services.Services.Seeding
{
    public interface IProductSeeder
    {
        public void Seed();
    }
}
